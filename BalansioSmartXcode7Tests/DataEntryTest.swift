//
//  DataEntryTest.swift
//  BalansioSmartXcode7
//
//  Created by Binh Tran on 14/11/2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import XCTest
@testable import BalansioSmartXcode7

class DataEntryTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // Test set everything ok
    func testNormal() {
        let dataEntry = DataEntry()
        let timestamp = NSDate()
        dataEntry.set(HealthDataType.BloodGlucose, timestamp: timestamp, value: 4.6, unit: "mmol/l")
        XCTAssert(dataEntry.dataType.rawValue == "Blood Glucose" && dataEntry.getTimestamp() == timestamp && dataEntry.getValue() == 4.6 && dataEntry.getUnit() == "mmol/l")
        
        GoalManager.sharedInstance.getGoalProgress(dataEntry)  { (progress, amount) in
            
            
        }
       
    }
    
}
