//
//  RuleTests.swift
//  BalansioSmartXcode7
//
//  Created by Binh Tran on 15/11/2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import XCTest
@testable import BalansioSmartXcode7

class RuleTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        let rule = Rule()
        rule.set(NotificationSetting.Easy)
        
        XCTAssert(rule.get().rawValue == "Easy")
    }
}
