//
//  ClinicalTests.swift
//  BalansioSmartXcode7
//
//  Created by Binh Tran on 09/11/2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import XCTest
@testable import BalansioSmartXcode7


class ClinicalTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    // Test when set min <= max
    func testNormal() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let clinical = Clinical()
        clinical.set(2.0, max: 3.0, unit: "")
        XCTAssert(clinical.getMax() == 3 && clinical.getMin() == 2.0, "Test set correct value")
        XCTAssert(clinical.getMax() >= clinical.getMin(), "Test max larger than min")
    }
    
    // Test when set min > max 
    // if min > max, then min = max = 0
    func testSpecialCase() {
        let clinical = Clinical()
        clinical.set(3.0, max: 1.0, unit: "")
        XCTAssertFalse(clinical.getMax() > clinical.getMin())
        XCTAssert(clinical.getMin() == clinical.getMax())
    }
    
    // Test input random number
    func testInputRandom() {
        let clinical = Clinical()
        let min = Double(arc4random())
        let max = Double(arc4random())
        clinical.set(min, max: max, unit: "")
        if min <= max {
            XCTAssert(clinical.getMax() >= clinical.getMin(), "Set the right min and max")
        } else {
            XCTAssert(clinical.getMin() == 0 && clinical.getMax() == 0, "If the inputs are not properly set, the min and max remain 0")
        }
    }
    
    // test input random 100 times to see the consistency
    func testInputRandom100Times() {
        var i = 0;
        while i < 100 {
            let clinical = Clinical()
            let min = Double(arc4random())
            let max = Double(arc4random())
            clinical.set(min, max: max, unit: "")
            if min <= max {
                XCTAssert(clinical.getMax() >= clinical.getMin(), "Set the right min and max")
            } else {
                XCTAssert(clinical.getMin() == 0 && clinical.getMax() == 0, "If the inputs are not properly set, the min and max remain 0")
            }
            
            i += 1
        }
    }
    
    
    
}
