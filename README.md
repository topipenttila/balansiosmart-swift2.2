## Balansio Smart - Healthcare application for diabetic people

- `Balansio Smart` is an assistive healthcare mobile application that helps the user to set healthy objectives and achieve them over time. 
- `Balansio Smart` is designed to help people with chronic illnesses make their day to day life easier.
- `Balansio Smart` is your personal self-care assistant. It allows you  to set up healthy goals and tracks your progress along the way.

## Team members:
- Phan Dang Hai
- Topi Penttilä
- Alex Lindroos
- Thanh Binh Tran

## Screenshots

![alt tag](/Screenshots/screenshot1.png)
![alt tag](/Screenshots/screenshot2.png)
![alt tag](/Screenshots/screenshot3.png)
![alt tag](/Screenshots/screenshot4.png)
![alt tag](/Screenshots/screenshot5.png)

## Core features

- Allow the user to create healthy goals with Goal Composer.
- Propose rule-based notification mechanisms for each goal
- Monitor the user’s progress in achieving goals.
- React to changes in progress according to assigned notification mechanisms.
- Display a visualization of the objectives and current state of the user’s progress in the Progress View.

## How to build

1) Clone the repository

```bash
$ git clone git@bitbucket.org:alexlindroos/balansiosmart-swift2.2.git
```

2) Install pods

```bash
$ pod install
```
3) Open the workspace in Xcode

```bash
$ open BalansioSmartXcode7.xcworkspace
```
4) Compile and run the app in your devices

## Code style

This project is following the [GitHub Swift Styleguide](https://github.com/github/swift-style-guide)

## Project Structure
```
BalansioSmartXCode7
├──NotificationManagers
├──Unit
├──Walkthrough
├──GoalDetail
├──ProgressView
├──GoalComposer
├──HealthKit
├──Storage
│   ├──Goal

```
## Libraries used

- [Realm](https://github.com/realm/realm-cocoa)
- [MBCircularProgressBar](https://github.com/MatiBot/MBCircularProgressBar)