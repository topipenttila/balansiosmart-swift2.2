//
//  HealthKitService.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 31/10/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import HealthKit
import UIKit
import RealmSwift

class HealthKitService {
    
    static let sharedInstance = HealthKitService()
    
    var healthStore: HKHealthStore = HKHealthStore()
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    // Set all supported data types
    var readTypes: Set<HKObjectType> = [
        HKObjectType.workoutType(),
        HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBloodGlucose)! as HKQuantityType,
        HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryCarbohydrates)! as HKQuantityType,
        HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)! as HKQuantityType,
        HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBloodPressureSystolic)! as HKQuantityType,
        HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBloodPressureDiastolic)! as HKQuantityType
    ]
    
    var backgroundQueries: [HKObserverQuery] = []
    
    private init() {
    }
    
    @available(iOS 8, *)
    private func setQueryAnchor(sampleType: HKSampleType, anchor: Int) {
        // store anchors
        defaults.setValue(String(anchor), forKey: sampleType.identifier + "_int")
    }
    
    @available(iOS 9, *)
    private func setQueryAnchor(sampleType: HKSampleType, anchror: HKQueryAnchor) {
        let data = NSKeyedArchiver.archivedDataWithRootObject(anchror)
        defaults.setObject(data, forKey: sampleType.identifier + "_data")
    }
    
    @available(iOS 8, *)
    private func getQueryAnchoriOS8(sampleType: HKSampleType) -> Int? {
        var anchor: Int? = nil
        if let stringValue = defaults.stringForKey(sampleType.identifier + "_int"), let value = Int(stringValue) {
            anchor = value
        }
        return anchor
    }
    
    @available(iOS 9, *)
    private func getQueryAnchor(sampleType: HKSampleType) -> HKQueryAnchor? {
        var anchor: HKQueryAnchor? = nil
        
        // Prior data
        if let data = defaults.objectForKey(sampleType.identifier + "_data") as? NSData {
            anchor = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? HKQueryAnchor
        }
        
        // If anhor wasn't stored as data, maybe it is stored as int
        if anchor == nil, let stringValue = defaults.stringForKey(sampleType.identifier + "_int"), let value = Int(stringValue) {
            anchor = HKQueryAnchor(fromValue: value)
        }
        
        return anchor
    }
    
    // Returns predicate which limits queries to return samples from healthkit actionvation data - 28 days to current time
    //    private func getPredicate() -> NSPredicate {
    //        let startDate = (getAuhtorizationTime() ?? NSDate()).dateByAddingTimeInterval(-HealthKitSettings.maximumTimeWindowInSeconds)
    //        let endDate = NSDate()
    //
    //        print("startDate: \(startDate), endDate: \(endDate)")
    //        return HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: [.StrictStartDate, .StrictEndDate])
    //    }
    
    // Returns query anchor for iOS8 devices. If query anchor exists then fetches are made from that point otherwise queries all items
    @available(iOS 8, *)
    private func getQueryAnchor(sampleType: HKSampleType, completionHandler: (HKAnchoredObjectQuery, [HKSample]?, Int, NSError?) -> Void) -> HKAnchoredObjectQuery {
        return HKAnchoredObjectQuery(type: sampleType, predicate: nil, anchor: getQueryAnchoriOS8(sampleType) ?? Int(HKAnchoredObjectQueryNoAnchor), limit: HKObjectQueryNoLimit, completionHandler: completionHandler)
    }
    
    // Returns query anchor for new devices. If query anchor exists then fetches are made from that point otherwise queries all items
    @available(iOS 9, *)
    private func getQueryAnchor(sampleType: HKSampleType, resultHandler: (HKAnchoredObjectQuery, [HKSample]?, [HKDeletedObject]?, HKQueryAnchor?, NSError?) -> Void) -> HKAnchoredObjectQuery {
        return HKAnchoredObjectQuery(type: sampleType, predicate: nil, anchor: getQueryAnchor(sampleType) ?? HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor)), limit: HKObjectQueryNoLimit, resultsHandler: resultHandler)
    }
    
    // Authorization setter. Authorization date is used for limiting start time in predicate creation
    //    func setAuthorizationTime(date: NSDate) {
    //        if Storage.getValue(.healthKitEnabledTime) == nil {
    //            Storage.setValue(String(date.timeIntervalSince1970), key: .healthKitEnabledTime)
    //        }
    //    }
    
    // Authorization getter. Authorization date is used for limiting start time in predicate creation
    //    func getAuhtorizationTime() -> NSDate? {
    //        var authorizationTime: NSDate? = nil
    //        if let timeStr = Storage.getValue(.healthKitEnabledTime), let timeInDouble = Double(timeStr) {
    //            authorizationTime = NSDate(timeIntervalSince1970: timeInDouble)
    //        }
    //
    //        return authorizationTime
    //    }
    
    // Starts authorization
    func requestAuthorization(completion: (Bool, NSError?) -> Void) {
        if HKHealthStore.isHealthDataAvailable() {
            authorizeHealthKit(completion)
        }
        else {
            //TODO better error handling
            completion(false, nil)
        }
    }
    
    func isHealthKitSupported() -> Bool {
        return HKHealthStore.isHealthDataAvailable()
    }
    
    // Does actual authorization
    private func authorizeHealthKit(completion: (Bool, NSError?) -> Void) {
        // Return an error if the HKStore is not available
        if HKHealthStore.isHealthDataAvailable() {
            // Request HK authorization
            healthStore.requestAuthorizationToShareTypes(nil, readTypes: readTypes) { (success, error) in
                completion(success, error)
            }
        }
        else {
            //TODO better error handling
            completion(false, nil)
        }
    }
    
    // Starts monitoring for requested types
    func startMonitoring() {
        if HKHealthStore.isHealthDataAvailable() {
            for item in readTypes {
                healthStore.enableBackgroundDeliveryForType(item, frequency: .Immediate) { (success, error) in
                    if success {
                        self.queryItemsFromHealthKitByType(item)
                    }
                }
            }
        }
    }
    
    // Stops monitoring
    func stopMonitoring() {
        if HKHealthStore.isHealthDataAvailable() {
            healthStore.disableAllBackgroundDeliveryWithCompletion() { (success, error) in
                NSLog("monitoring disabled with status: \(success)")
            }
            
            // Stop background queries
            for backgroundQuery in backgroundQueries {
                healthStore.stopQuery(backgroundQuery)
            }
            backgroundQueries = []
        }
    }
    
    // Starts to observing specific sample types from health kit and get notifies if item changes
    func queryItemsFromHealthKitByType(type: HKObjectType) {
        
        var sampleType: HKSampleType?
        if let workoutType = type as? HKWorkoutType {
            sampleType = workoutType
        }
        else {
            sampleType = HKObjectType.quantityTypeForIdentifier(type.identifier)
        }
        
        if let sampleType = sampleType {
            let query = HKObserverQuery(sampleType: sampleType, predicate: nil) { (query, completionHandler, error) in
                if error != nil {
                    abort()
                }
                
                if sampleType == HKObjectType.workoutType() {
                        self.synchronizeWorkoutData(sampleType)
                } else {
                        self.synchronizeData(sampleType)
                    
                }
                
                
                // complete execution
                completionHandler()
            }
            
            backgroundQueries.append(query)
            healthStore.executeQuery(query)
        }
    }
    
    // Uses apis which are available for iOS 8 devices
    @available(iOS 8, *)
    func synchronizeWorkoutDataiOS8(sampleType: HKSampleType) {
        
        let completionHandler: (HKAnchoredObjectQuery, [HKSample]?, Int, NSError?) -> Void = {
            (query, samples, anchor, error) in
            do {
                try self.handleWorkoutData(samples, error: error)
                
                // Update queryAnchor when data is stored successfully and there was a data otherwise data is not received anymore due to updated anchor
                if samples?.count > 0 {
                    self.setQueryAnchor(sampleType, anchor: anchor)
                }
            }
            catch let error {
                NSLog("Error occured while fetching date from HealtKit, anchor not updated due to: \((error as NSError).description)")
            }
        }
        
        let queryAnchor = getQueryAnchor(sampleType, completionHandler: completionHandler)
        healthStore.executeQuery(queryAnchor)
    }
    
    // Uses newer apis which are available from iOS 9
    @available(iOS 9, *)
    func synchronizeWorkoutData(sampleType: HKSampleType) {
        
        let resultHandler: (HKAnchoredObjectQuery, [HKSample]?, [HKDeletedObject]?, HKQueryAnchor?, NSError?) -> Void = {
            (query, samples, deletedObjects, anchor, error) in
            
            do {
                try self.handleWorkoutData(samples, error: error)
                
                // Update queryAnchor when data is stored successfully and there was a data otherwise data is not received anymore due to updated anchor
                if samples?.count > 0, let anchor = anchor {
                    self.setQueryAnchor(sampleType, anchror: anchor)
                }
            }
            catch let error {
                NSLog("Error occured while fetching date from HealtKit, anchor not updated due to: \((error as NSError).description)")
            }
        }
        
        let queryAnchor = getQueryAnchor(sampleType, resultHandler: resultHandler)
        healthStore.executeQuery(queryAnchor)
    }
    
    // Uses apis which are available for iOS 8 devices
    @available(iOS 8, *)
    func synchronizeDataiOS8(sampleType: HKSampleType) {
        
        let completionHandler: (HKAnchoredObjectQuery, [HKSample]?, Int, NSError?) -> Void = {
            (query, samples, anchor, error) in
            do {
                print("handele data")
                
                // Update queryAnchor when data is stored successfully and there was a data otherwise data is not received anymore due to updated anchor
                if samples?.count > 0 {
                    self.setQueryAnchor(sampleType, anchor: anchor)
                    print("anchor set")
                }
            }
            catch let error {
                NSLog("Error occured while fetching date from HealtKit, anchor not updated due to: \((error as NSError).description)")
            }
        }
        
        let queryAnchor = getQueryAnchor(sampleType, completionHandler: completionHandler)
        healthStore.executeQuery(queryAnchor)
    }
    
    // Uses newer apis which are available from iOS 9
    @available(iOS 9, *)
    func synchronizeData(sampleType: HKSampleType) {
        
        let resultHandler: (HKAnchoredObjectQuery, [HKSample]?, [HKDeletedObject]?, HKQueryAnchor?, NSError?) -> Void = {
            (query, samples, deletedObjects, anchor, error) in
            
            do {
                try self.handleData(samples, error: error)
                
                // Update queryAnchor when data is stored successfully and there was a data otherwise data is not received anymore due to updated anchor
                if samples?.count > 0, let anchor = anchor {
                    self.setQueryAnchor(sampleType, anchror: anchor)
                    print("anchor set")
                }
            }
            catch let error {
                NSLog("Error occured while fetching date from HealtKit, anchor not updated due to: \((error as NSError).description)")
            }
        }
        
        let queryAnchor = getQueryAnchor(sampleType, resultHandler: resultHandler)
        healthStore.executeQuery(queryAnchor)
    }
    
    //Data handling for every iOS version
    func handleData(samples: [HKSample]?, error: NSError?) throws {
        if let error = error {
            NSLog("Error occured while fetching data due to: \(error.description)")
            abort()
        }
        if let samples = samples {
            for sample in samples {
                if let sample = sample as? HKQuantitySample {
                   
                    print(sample.quantityType)
                    
                    let quantityType = String(sample.quantityType)
                    switch quantityType {
                    case HKQuantityTypeIdentifierBloodGlucose:
                        let mmolUnit = HKUnit.moleUnitWithMetricPrefix(HKMetricPrefix.Milli, molarMass: HKUnitMolarMassBloodGlucose).unitDividedByUnit(HKUnit.literUnit())
                        let dataEntry = DataEntry()
                        // Round valu to 2 digits
                        let value = sample.quantity.doubleValueForUnit(mmolUnit).roundToPlaces(1)
                        //Convert
                        dataEntry.set(HealthDataType.BloodGlucose, timestamp: sample.startDate, value: value, unit: "mmol/l")
                        print(dataEntry)
                        dispatch_async(dispatch_get_main_queue(), {
                            RealmManager.sharedInstance.saveDataEntryToRealm(dataEntry)
                        })
                        

                    case HKQuantityTypeIdentifierBodyMass:
                        let dataEntry = DataEntry()
                        let kgUnit = HKUnit.gramUnitWithMetricPrefix(HKMetricPrefix.Kilo)
                        dataEntry.set(HealthDataType.Weight, timestamp: sample.startDate, value: sample.quantity.doubleValueForUnit(kgUnit), unit: kgUnit.unitString)
                        dispatch_async(dispatch_get_main_queue(), {
                            RealmManager.sharedInstance.saveDataEntryToRealm(dataEntry)
                        })
                    default:
                        break
                    }
                    //RealmManager.sharedInstance.syncAndSaveData(dataEntry) <- if server available

                    
                    
                }
            }
        }
    }
    
    //Data handling for every iOS version
    func handleWorkoutData(samples: [HKSample]?, error: NSError?) throws {
        if let error = error {
            NSLog("Error occured while fetching workout data due to: \(error.description)")
            abort()
        }
    }
}
