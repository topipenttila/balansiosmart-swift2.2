//
//  Goal.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 01/11/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import RealmSwift
import HealthKit

class Goal: Object {
    //TODO: Clinical & Discipline as separate Goals
    
    private dynamic var id: String = NSUUID().UUIDString
    private dynamic var rawDataType = ""
    internal var dataType: HealthDataType {
        get {
            return HealthDataType(rawValue: rawDataType)!
        }
        set {
            rawDataType = newValue.rawValue
        }
    }
    private dynamic var discipline: Discipline?
    private dynamic var clinical: Clinical?
    private dynamic var rule: Rule?
    private dynamic var timestamp: NSDate?
    
    override static func primaryKey() -> String? {
        return "rawDataType"
    }
    
    //Set the values either one by one or many at once
    func set(dataType: HealthDataType?, discipline: Discipline?, clinical: Clinical?, rule: Rule?, timestamp: NSDate?) -> Goal {
        // Set values if != nil
        if dataType != nil {
            self.dataType = dataType!
        }
        if discipline != nil {
            self.discipline = discipline
        }
        if clinical != nil {
            self.clinical = clinical
        }
        if rule != nil {
            self.rule = rule
        }
        if timestamp != nil {
            self.timestamp = timestamp
        }
        
        return self
    }
    
    
    func getGoalId() -> String {
        return self.id
    }
    
    func getDataType() -> HealthDataType? {
        return self.dataType
    }
    
    func getDiscipline() -> Discipline? {
        return self.discipline
    }
    
    func getClinical() -> Clinical? {
        return self.clinical
    }
    
    func getRule() -> Rule? {
        return self.rule
    }
    
    func getTimestamp() -> NSDate? {
        return self.timestamp
    }
    
    //Persist the data with Realm
    func save() {
        // Get the default Realm
        let realm = try! Realm()
        // Add to Realm
        try! realm.write {
            realm.add(self, update: true)
            
        }
        // Schedule reminder timing
        if self.getRule()?.get() == .Strict {
            //ReminderNotificationManager.sharedInstance.scheduleDefaultDisciplineReminderTiming(self)
            ReminderNotificationManager.sharedInstance.scheduleDemoDefaultReminderTiming(self)
        }
    }
    
    override static func ignoredProperties() -> [String] {
        return ["dataType"]
    }
    
}
