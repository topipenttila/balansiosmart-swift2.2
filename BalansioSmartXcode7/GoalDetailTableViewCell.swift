//
//  GoalDetailTableViewCell.swift
//  BalansioSmartXcode7
//
//  Created by iosdev on 6.12.2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit

class GoalDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
