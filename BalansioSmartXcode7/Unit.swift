//
//  Unit.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 07/12/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return round(self * divisor) / divisor
    }
}
