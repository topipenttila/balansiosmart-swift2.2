//
//  SelectInputIntensityViewController.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 11/1/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit

class SelectInputIntensityViewController: UIViewController, UITextFieldDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var optionTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var continueButton: UIButton!
    
    //MARK: Properties
    weak var goalComposerContainerVC: GoalComposerContainerViewController?
    let option = [NSLocalizedString("day", comment: ""), NSLocalizedString("week", comment: ""), NSLocalizedString("month", comment: ""), NSLocalizedString("year", comment: "")]
    private var optionPickerView: UIPickerView = UIPickerView()
    
    
    //MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        goalComposerContainerVC = parentViewController as? GoalComposerContainerViewController
        if let type = goalComposerContainerVC?.goal.getDataType()!.rawValue {
            instructionLabel.text = NSLocalizedString("intensity_text", comment: "") + (type.lowercaseString)

        }
        optionPickerView.dataSource = self
        optionPickerView.delegate = self
        
        let dismissKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(SelectInputIntensityViewController.dismissKeyboard))
        view.addGestureRecognizer(dismissKeyboardGesture)
        
        amountTextField.delegate = self
        
        if goalComposerContainerVC?.mode == .Edit {
            if let amount = goalComposerContainerVC?.editingGoal?.getDiscipline()?.getAmount(), let period = goalComposerContainerVC?.editingGoal?.getDiscipline()?.getPeriod() {
                amountTextField.text = String(amount)
                optionTextField.text = String(period)
            }
        }
        
        continueButton.setTitle(NSLocalizedString("continue", comment: ""), forState: UIControlState.Normal)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func didTapDismiss(sender: UIBarButtonItem) {
        goalComposerContainerVC?.dismissViewControllerAnimated(true, completion: nil)
    }
}

//MARK: Routing
extension SelectInputIntensityViewController {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "FromInputIntensityToRange" {
            guard let amount = amountTextField.text where !amount.isEmpty, let period = optionTextField.text where !period.isEmpty else {return}
            let discipline = Discipline()
            discipline.set(Int(amount)!, period: Period.periodFromString(period.lowercaseString)!)
            goalComposerContainerVC?.goal.set(nil, discipline: discipline, clinical: nil, rule: nil, timestamp: nil)
        }
    }
}

//MARK: UITextFieldDelegate
extension SelectInputIntensityViewController {
    @IBAction func optionDidBeginEditing(sender: UITextField) {
        sender.inputView = optionPickerView
        if optionPickerView.selectedRowInComponent(0) == 0 {
            pickerView(optionPickerView, didSelectRow: 0, inComponent: 0)
        }
    }
}

//MARK: UIPickerViewDelegate, UIPickerViewDataSource
extension SelectInputIntensityViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return option.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return option[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        optionTextField.text = option[row]
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange,
                   replacementString string: String) -> Bool
    {
        let maxLength = 3
        let currentString: NSString = textField.text!
        let newString: NSString =
            currentString.stringByReplacingCharactersInRange(range, withString: string)
        return newString.length <= maxLength
    }
    
}
