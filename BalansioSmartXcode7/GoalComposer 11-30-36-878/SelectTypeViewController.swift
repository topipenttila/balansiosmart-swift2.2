//
//  SelectTypeViewController.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 11/1/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit
import RealmSwift

class SelectTypeViewController: UIViewController {

    //MARK: Properties
    weak var goalComposerContainerVC: GoalComposerContainerViewController?
    
    //MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //displayWalkthroughs()
        goalComposerContainerVC = parentViewController as? GoalComposerContainerViewController
        if goalComposerContainerVC?.mode == .Edit {
            if let type = goalComposerContainerVC?.editingGoal?.dataType {
                performSegueWithIdentifier("FromSelectTypeToSelectInputIntensity", sender: type.rawValue)
            }
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func didSelectType(sender: UIButton) {
        guard let type = sender.titleLabel?.text else {return}
        performSegueWithIdentifier("FromSelectTypeToSelectInputIntensity", sender: type)
    }
    @IBAction func didTapDismiss(sender: UIBarButtonItem) {
        goalComposerContainerVC?.dismissViewControllerAnimated(true, completion: nil)
    }
}


//MARK: Routing
extension SelectTypeViewController {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "FromSelectTypeToSelectInputIntensity" {
            if let type = sender as? String {
                goalComposerContainerVC?.goal.set(HealthDataType.healthDataTypeFromString(type), discipline: nil, clinical: nil, rule: nil, timestamp: nil)
            }
        }
    }
    
}

