//
//  GoalOverviewViewController.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 12/5/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit

class GoalOverviewViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var disciplineLabel: UILabel!
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var motivationLabel: UILabel!
    
    weak var goalComposerContainerVC: GoalComposerContainerViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        goalComposerContainerVC = parentViewController as? GoalComposerContainerViewController
        
        if let amount = goalComposerContainerVC?.goal.getDiscipline()?.getAmount(), period = goalComposerContainerVC?.goal.getDiscipline()?.getPeriod() {
            disciplineLabel.text = "\(amount) " + NSLocalizedString("intensity_text2", comment: "") + " \(period)"
        }
        
        if let min = goalComposerContainerVC?.goal.getClinical()?.getMin(), max = goalComposerContainerVC?.goal.getClinical()?.getMax(), unit =  goalComposerContainerVC?.goal.getClinical()?.getUnit() {
            targetLabel.text = "\(min) - \(max) \(unit)"
        }
        if let notification = goalComposerContainerVC?.goal.getRule()?.get().rawValue {
            notificationLabel.text = notification
        }
        if goalComposerContainerVC?.mode == .Edit {
            createButton.setTitle(NSLocalizedString("save", comment: ""), forState: .Normal)
        }else{
            createButton.setTitle(NSLocalizedString("create", comment: ""), forState: UIControlState.Normal)
        }
        
        motivationLabel.text = NSLocalizedString("motivation_text", comment: "")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapCreateGoal(sender: UIButton) {
        if goalComposerContainerVC?.mode == .Create {
            createNewGoal()
        } else {
            saveEditedGoal()
        }
    }

    @IBAction func didTapDismiss(sender: UIBarButtonItem) {
        goalComposerContainerVC?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func createNewGoal() {
        //PersistGoal
        goalComposerContainerVC?.persistGoal()
        //Show success alert
        let successAlertVC = UIAlertController(title: nil, message: NSLocalizedString("success", comment: ""), preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("dismiss", comment: ""), style: .Cancel, handler: { [unowned self] _ in
            //            self.goalComposerContainerVC?.goal = Goal()
            self.goalComposerContainerVC?.dismissViewControllerAnimated(true, completion: {
                NSNotificationCenter.defaultCenter().postNotificationName("refreshProgressView", object: nil)
            })
            })
        successAlertVC.addAction(cancelAction)
        presentViewController(successAlertVC, animated: true, completion: nil)
    }
    
    func saveEditedGoal() {
        goalComposerContainerVC?.saveGoal()
        //Show success alert
        let successAlertVC = UIAlertController(title: nil, message: NSLocalizedString("success2", comment: ""), preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("dismiss", comment: ""), style: .Cancel, handler: { [unowned self] _ in
            //            self.goalComposerContainerVC?.goal = Goal()
            self.goalComposerContainerVC?.dismissViewControllerAnimated(true, completion: {
                NSNotificationCenter.defaultCenter().postNotificationName("refreshProgressView", object: nil)
                NSNotificationCenter.defaultCenter().postNotificationName("refreshDetailView", object: nil)
            })
            })
        successAlertVC.addAction(cancelAction)
        presentViewController(successAlertVC, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
