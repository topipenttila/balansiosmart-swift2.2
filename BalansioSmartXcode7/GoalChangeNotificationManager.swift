//
//  GoalChangeNotificationManager.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 12/12/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import UserNotifications

class GoalChangeNotificationManager {
    
    static let sharedInstance: GoalChangeNotificationManager = GoalChangeNotificationManager()
    
    func fireGoalChangeNotificationIfNeeded(dataType: HealthDataType) {
        // Check if all entries are not in range
        func areNotInRange(entries: [DataEntry]) -> Bool {
            let clinical = GoalManager.sharedInstance.getGoalForDataType((entries.first?.getDataType())!)?.getClinical()
            for entry in entries {
                if (entry.getValue() <= clinical?.getMax() && entry.getValue() >= clinical?.getMin()) {
                    return false
                }
            }
            return true
        }
        
        RealmManager.sharedInstance.getLatestDataEntries(3, dataType: dataType) { (latest) -> () in
            var sentTimestamp = 0.0
            if NSUserDefaults.standardUserDefaults().objectForKey("changeNotificationSent") != nil {
                sentTimestamp = NSUserDefaults.standardUserDefaults().objectForKey("changeNotificationSent") as! NSTimeInterval
            }
            
            //Schedule notification if 3 latest are in range & change notification not sent for one day
            if areNotInRange(latest) && (sentTimestamp + 86400) < NSDate().timeIntervalSince1970 {
                if #available(iOS 10.0, *) {
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10.0, repeats: false)
                    let notificationContent = UNMutableNotificationContent()
                    notificationContent.title = dataType.rawValue
                    notificationContent.body = "Your " + dataType.rawValue + " values have recently been out of the desired range. Would you like to change your goal?"
                    notificationContent.categoryIdentifier = dataType.rawValue
                    
                    let request = UNNotificationRequest(
                        identifier: dataType.rawValue,
                        content: notificationContent,
                        trigger: trigger
                    )
                    UNUserNotificationCenter.currentNotificationCenter().addNotificationRequest(request, withCompletionHandler: nil)
                    print("scheduled change notification" + String(request))
                    
                    //Save to Realm
                    let notification: Notification = Notification()
                    let dateFire = NSDate().dateByAddingTimeInterval(10)
                    notification.set(request.identifier, dataType: dataType, timestamp: dateFire, notificationType: .Motivational)
                    RealmManager.sharedInstance.saveNotificationToRealm(notification)
                    NSUserDefaults.standardUserDefaults().setDouble(NSDate().timeIntervalSince1970, forKey: "changeNotificationSent")
                    
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
}
