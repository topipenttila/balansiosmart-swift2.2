//
//  NotificationViewController.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 11/24/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit
import UserNotifications

class NotificationViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var notificationCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //MARK: Layout for CardPaymentCollectionView
    private let notificationItemWidth = CGSize.screenWidth()*0.8
    private let notificationItemHeight = CGSize.screenHeight()*0.25
    private let minimumLineSpacing = CGSize.screenWidth()*0.075
    private let insetSectionLeft = CGSize.screenWidth()*0.1
    
    private lazy var emptyDataSourceView: UILabel = {
        let label = UILabel()
        label.text = "You don't have any notifications"
        label.textColor = UIColor.whiteColor()
        label.font = UIFont(name: "Avenir", size: 20)
        label.textAlignment = .Center
        return label
    }()
    
    //MARK: Properties
    private var unreactedNotifications = []
    
    //MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationCollectionView.delegate = self
        notificationCollectionView.dataSource = self
        reloadNotificationDataSource()
        
        //Observer
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NotificationViewController.reloadNotificationDataSource), name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        reloadNotificationDataSource()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadNotificationDataSource() {
        if #available(iOS 10.0, *) {
            NotificationManager.sharedInstance.getUnreactedNotificationsByDataTypeFromDate(NSDate().dateByAddingTimeInterval(-86400.0), completionHandler: { [unowned self] (unreactedNotifications) in
                self.unreactedNotifications = unreactedNotifications
                dispatch_async(dispatch_get_main_queue(), {
                    self.updateNotificationCollectionView()
                })
                })
        } else {
            // Fallback on earlier versions
        }
    }
    
    func updateNotificationCollectionView() {
        emptyDataSourceView.removeFromSuperview()
        if collectionView(notificationCollectionView, numberOfItemsInSection: 0) == 0 {
            view.addSubview(emptyDataSourceView)
            emptyDataSourceView.frame = view.bounds
            notificationCollectionView.alpha = 0
        } else {
            notificationCollectionView.alpha = 1
            notificationCollectionView.reloadData()
        }
    }

}

//MARK: UICollectionViewFlowLayout
extension NotificationViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: notificationItemWidth, height: notificationItemHeight)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return minimumLineSpacing
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: insetSectionLeft, bottom: 10, right: CGSize.screenWidth()*0.1)
    }
}

//MARK: UICollectionViewDelegate
extension NotificationViewController: UICollectionViewDelegate {
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageWidth:Float = Float(notificationItemWidth + minimumLineSpacing)
        let currentOffSet:Float = Float(scrollView.contentOffset.x)
        let targetOffSet:Float = Float(targetContentOffset.memory.x)
        
        var newTargetOffset:Float = 0
        
        if(targetOffSet > currentOffSet){
            newTargetOffset = ceilf(currentOffSet / pageWidth) * pageWidth
        }else{
            newTargetOffset = floorf(currentOffSet / pageWidth) * pageWidth
        }
        
        if(newTargetOffset < 0){
            newTargetOffset = 0
        }else if (newTargetOffset > Float(scrollView.contentSize.width)){
            newTargetOffset = Float(scrollView.contentSize.width)
        }
        
        targetContentOffset.memory.x = CGFloat(currentOffSet)
        scrollView.setContentOffset(CGPointMake(CGFloat(newTargetOffset), 0), animated: true)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / notificationItemWidth)
    }
}

extension NotificationViewController: UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = unreactedNotifications.count
        return unreactedNotifications.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("notificationViewCell", forIndexPath: indexPath) as! NotificationCollectionViewCell
        if #available(iOS 10.0, *) {
            let unreactedNotification = unreactedNotifications[indexPath.row] as! UNNotification
            cell.configure(unreactedNotification)
        } else {
            // Fallback on earlier versions
        }
        
        return cell
    }
}
