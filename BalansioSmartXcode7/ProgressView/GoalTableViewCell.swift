//
//  GoalTableViewCell.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 11/25/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit
import MBCircularProgressBar
import RealmSwift

class GoalTableViewCell: UITableViewCell {
    @IBOutlet weak var dataTypeImageView: UIImageView!
    @IBOutlet weak var dataTypeLabel: UILabel!
    @IBOutlet weak var circularProgressBar: MBCircularProgressBarView!
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var periodLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /**
     * Description: Configure the display for each goal cell in the tableView
     * Parameters: Goal object
     */
    func configure(goal: Goal) {
        guard goal.getDataType() != nil else {return}
        let dataType = goal.getDataType()!
        
        // Set data type name for labelText
        dataTypeLabel.text = dataType.rawValue
        
        // get last data entry for the goal
        let realm = try! Realm()
        let lastDataEntry = realm.objects(DataEntry).filter("rawDataType == '\(dataType.rawValue)'").last

        // Configure the circular progress bar and thumbs up/down based on last data entry
        // If there is no last dataEntry -> thumb down
        if lastDataEntry?.getDataType() != nil {
            GoalManager.sharedInstance.getGoalProgress((lastDataEntry)!) { (progress, amount) in
                guard let amount = amount else {return}
                
                // Set the values for circular progress bar
                self.circularProgressBar.maxValue = CGFloat(amount)
                self.circularProgressBar.value = CGFloat(progress)
                self.circularProgressBar.unitString = "/\(String(amount))"
                
                // Set thumb up/down based on the last dataEntry value
                // in range -> thumb up
                // out range -> thumb down
                let dataEntryValue = lastDataEntry?.getValue()
                if dataEntryValue<=goal.getClinical()?.getMax() && dataEntryValue>=goal.getClinical()?.getMin() {
                    self.thumbImageView.image = UIImage(named: "thumb_up")
                } else {
                    self.thumbImageView.image = UIImage(named: "thumb_down")
                }
                
                // Set thumb up/down for Weight based on default user's weight
                // if the different between weights is equal or less than the 
                // max set in clinical, then show thumb_up, otherwise thumb_down
                if (dataType == HealthDataType.Weight) {
                    let user = realm.objects(User).first
                    let diff = dataEntryValue! - (user?.weight)!
                    if (diff <= goal.getClinical()?.getMax()) {
                        self.thumbImageView.image = UIImage(named: "thumb_up")
                    } else {
                        self.thumbImageView.image = UIImage(named: "thumb_down")
                    }
                    // update user's weight in realm database
                    try! realm.write {
                        user!.weight = dataEntryValue!
                    }

                }
                
            }
        } else {
            thumbImageView.image = UIImage(named: "thumb_down")
            guard let amount = goal.getDiscipline()?.getAmount() else {return}
            self.circularProgressBar.maxValue = CGFloat(amount)
            self.circularProgressBar.value = CGFloat(0)
            self.circularProgressBar.unitString = "/\(amount)"
        }
        
        // set unit inside circular progress bar e.g "per week"
        if let period = goal.getDiscipline()?.getPeriod() {
            periodLabel.text = NSLocalizedString("per", comment: "") + (period.rawValue)
        }
        
        RealmManager.sharedInstance.getNotificationAmountFromDateByDataType(NSDate(), dataType: dataType) { (amount) in
            print("\(dataType) number of notifications: \(amount)")
        }
        
        // Configure back ground color based on dataType
        dataTypeImageView.image = UIImage(named: "\(String(dataType).lowercaseString)_icon")
        switch dataType {
        case .BloodGlucose:
            backgroundColor = UIColor(red: 243/255, green: 92/255, blue: 82/255, alpha: 1)
        case .A1c:
            backgroundColor = UIColor(red: 52/255, green: 127/255, blue: 196/255, alpha: 1)
        case .Activity:
            backgroundColor = UIColor(red: 26/255, green: 188/255, blue: 156/255, alpha: 1)
        case .BloodPressureSystolic:
            backgroundColor = UIColor(red: 81/255, green: 197/255, blue: 159/255, alpha: 1)
        case .BloodPressureDiastolic:
            backgroundColor = UIColor(red: 238/255, green: 0/255, blue: 238/255, alpha: 1)
        case .Sleep:
            backgroundColor = UIColor(red: 243/255, green: 156/255, blue: 18/255, alpha: 1)
        case .Weight:
            backgroundColor = UIColor(red: 155/255, green: 89/255, blue: 182/255, alpha: 1)
        default:
            break
        }
    }

}
