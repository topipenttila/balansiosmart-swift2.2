//
//  AppDelegate.swift
//  BalansioSmartXcode7
//
//  Created by iosdev on 28.10.2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit
import RealmSwift
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    let uiRealm = try! Realm()
    
    var window: UIWindow?
    
    let healthKitService = HealthKitService.sharedInstance
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        // Start monitoring - HealthKit
        healthKitService.requestAuthorization () { (success, error) in
            if success {
                self.healthKitService.startMonitoring()
            } else {
                // Stop monitorig
                self.healthKitService.stopMonitoring()
            }
        }
        
        if #available(iOS 10.0, *) {
            let options: UNAuthorizationOptions = [.Alert, .Badge, .Sound]
            UNUserNotificationCenter.currentNotificationCenter().requestAuthorizationWithOptions(options, completionHandler: { (granted,error) in
                }
            )
        } else {
            // Fallback on earlier versions
        }
        
        //RealmManager.sharedInstance.setupRealm() <-- if server available
        //Set background fetch minimum interval
        UIApplication.sharedApplication().setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        
        // Display introduction to the application only once
        // and in the walkthrough introduction, the user can
        // choose default goals or create custom goals
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let displayedWalkthrough = userDefaults.boolForKey("DisplayedWalkthrough")
        if !displayedWalkthrough {
            let pageViewController = UIStoryboard(name: "GoalComposer", bundle: nil).instantiateViewControllerWithIdentifier("PageViewController")
            window?.rootViewController = pageViewController
        }
        
        //Set UNUserNotificationCenterDelegate
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.currentNotificationCenter().delegate = self
        } else {
            // Fallback on earlier versions
        }
        
        // Add default user
        DbUtils.addOrUpdateSingleUser(User())
        
        RealmManager.sharedInstance.getNotificationAmountFromDateByDataType(NSDate().dateByAddingTimeInterval(-3600), dataType: HealthDataType.BloodGlucose) { (amount) -> () in
            print("amount" + String(amount))
        }
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // Support for background fetch
    func application(application: UIApplication, performFetchWithCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        print("background fetch")
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(center: UNUserNotificationCenter, didReceiveNotificationResponse response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void) {
        print("didReceiveNotif")
        
        
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(center: UNUserNotificationCenter, willPresentNotification notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void) {
        
    }
}


