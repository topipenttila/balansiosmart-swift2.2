//
//  GoalComposerContainerViewController.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 11/14/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit
import RealmSwift

enum GoalComposerMode {
    case Create
    case Edit
}

class GoalComposerContainerViewController: UINavigationController {

    var mode: GoalComposerMode = .Create
    var goal = Goal()
    var editingGoal: Goal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func persistGoal() {
        print(goal)
        goal.save()
    }
    
    func saveGoal() {
        let realm = try! Realm()
//        let editingGoal = realm.objects(Goal.self).filter("id = \(self.editingGoal?.getGoalId())").first!
        try! realm.write {
            editingGoal!.set(nil, discipline: goal.getDiscipline(), clinical: goal.getClinical(), rule: goal.getRule(), timestamp: goal.getTimestamp())
        }
    }
}
