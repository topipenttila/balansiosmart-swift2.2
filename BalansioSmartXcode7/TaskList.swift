//
//  TaskList.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 11/11/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import RealmSwift
import Foundation

final class TaskList: Object {
    dynamic var text = ""
    dynamic var id = ""
    let items = List<Task>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

final class Task: Object {
    dynamic var text = ""
    dynamic var completed = false
}
