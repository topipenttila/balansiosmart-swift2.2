//
//  RealmManager.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 14/11/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import RealmSwift
import Foundation

// Class for managing DataEntry & Notification persisting

class RealmManager {
    
    static let sharedInstance: RealmManager = RealmManager()
    
    var items = List<Task>()

    var realm = try! Realm()
    
    
    func setupRealm() {
        
        // Log in existing user with username and password
        let username = "topi.penttila@metropolia.fi"  // <--- Update this
        let password = "admin"  // <--- Update this
        let serverURL = NSURL(string: "http://127.0.0.1:9080")!
        
        
        SyncUser.logInWithCredentials(SyncCredentials.usernamePassword(username, password: password, register: false), authServerURL: serverURL, onCompletion: { user, error in
            guard let user = user else {
                fatalError(String(error))
            }
            
            let syncConfiguration = SyncConfiguration.init(user: user, realmURL: NSURL(string: "realm://127.0.0.1:9080/~/balasiosmarttii")!)
            // Open Realm
            let configuration = Realm.Configuration(
                syncConfiguration: syncConfiguration
            )
            print("conf " + String(configuration))
            self.realm = try! Realm(configuration: configuration)
            
            
            //            // Show initial data
            //            func updateList() {
            //                if self.items.realm == nil, let list = realm.objects(TaskList.self).first {
            //                    self.items = list.items
            //                }
            //                realm.objects(Discipline.self)
            //            }
            //            updateList()
            //
            //            let items = self.items
            //            let text = "testText"
            //            try! items.realm?.write {
            //                items.insert(Task(value: ["text": text]), atIndex: items.filter("completed = false").count)
            //            }
            //
        })
        
    }
    
    //Saves the dataEntry to the default local Realm and remotely to the server
    func syncAndSaveData(dataEntry: DataEntry) {
        // Log in existing user with username and password
        let username = "topi.penttila@metropolia.fi"  // <--- Update this
        let password = "admin"  // <--- Update this
        let serverURL = NSURL(string: "http://127.0.0.1:9080")!
        
        SyncUser.logInWithCredentials(SyncCredentials.usernamePassword(username, password: password, register: false), authServerURL: serverURL, onCompletion: { user, error in
            guard let user = user else {
                fatalError(String(error))
            }
            
            let syncConfiguration = SyncConfiguration.init(user: user, realmURL: NSURL(string: "realm://127.0.0.1:9080/~/balasiosmarttii")!)
            // Open Realm
            let configuration = Realm.Configuration(
                syncConfiguration: syncConfiguration
            )
            print("conf " + String(configuration))
            self.realm = try! Realm(configuration: configuration)
            GoalManager.sharedInstance.getGoalProgress(dataEntry) { (progress, amount) -> () in
                
            }
            
            try! self.realm.write {
                self.realm.add(dataEntry)
            }
            self.realm = try! Realm()
            try! self.realm.write {
                self.realm.add(dataEntry)
            }
            //            // Notify us when Realm changes
            //            self.notificationToken = realm.addNotificationBlock { _ in
            //                //updateList()
            //            }
        })
    }
    
    //Unused, gets the latest n amount of dataEntries from server, slow
    func getLatestDataEntriesFromServer(amount: Int, dataEntry: DataEntry, completion: (result: [DataEntry]) -> ()) {
        // Log in existing user with username and password
        let username = "topi.penttila@metropolia.fi"  // <--- Update this
        let password = "admin"  // <--- Update this
        let serverURL = NSURL(string: "http://127.0.0.1:9080")!
        SyncUser.logInWithCredentials(SyncCredentials.usernamePassword(username, password: password, register: false), authServerURL: serverURL, onCompletion: { user, error in
            guard let user = user else {
                fatalError(String(error))
            }
            
            let syncConfiguration = SyncConfiguration.init(user: user, realmURL: NSURL(string: "realm://127.0.0.1:9080/~/balasiosmarttii")!)
            // Open Realm
            let configuration = Realm.Configuration(
                syncConfiguration: syncConfiguration
            )
            print("conf " + String(configuration))
            self.realm = try! Realm(configuration: configuration)
            print("realmObjects" + String(self.realm.objects(DataEntry)))
            var entries: [DataEntry] = []
            for object in self.realm.objects(DataEntry) {
                var i = 0
                while i <= amount {
                    if object.getDataType() == dataEntry.getDataType() {
                        i += 1
                        entries.append(object)
                    }
                }
            }
            completion(result: entries)
            print(completion)
            
        })
        
    }
    
    //Gets the latest n amount of dataEntries from the default Realm
    func getLatestDataEntries(amount: Int, dataType: HealthDataType, completion: (result: [DataEntry]) -> ()) {
        self.realm = try! Realm()
        var entries: [DataEntry] = []
        var i = 0
        for object in self.realm.objects(DataEntry).sorted("timestamp", ascending: false) {
            if i < amount && object.getDataType() == dataType {
                i += 1
                if object.getDataType() == dataType {
                    entries.append(object)
                }
            }
        }
        completion(result: entries)
    }
    
    //Saves an Entry to the default Realm & fires motivational and goal change notifications if needed
    func saveDataEntryToRealm(dataEntry: DataEntry) {
        
        self.realm = try! Realm()
        try! self.realm.write {
            self.realm.add(dataEntry)
        }
        print("saved")
        NSNotificationCenter.defaultCenter().postNotificationName("refreshProgressView", object: nil)
        //Reschedule reminder notifications for DataType
        print(GoalManager.sharedInstance.getGoalSettingForDataType(dataEntry.getDataType()))
        if GoalManager.sharedInstance.getGoalSettingForDataType(dataEntry.getDataType()) == .Strict {
            ReminderNotificationManager.sharedInstance.rescheduleNotification(dataEntry)
        }
        MotivationalNotificationManager.sharedInstance.fireMotivationalNotificationIfNeeded(dataEntry.getDataType())
        GoalChangeNotificationManager.sharedInstance.fireGoalChangeNotificationIfNeeded(dataEntry.getDataType())
    }
    
    func saveNotificationToRealm(notification: Notification) {
        self.realm = try! Realm()
        try! self.realm.write {
            self.realm.add(notification)
        }
        
    }
    
    //Gets the past Notifications from Realm
    func getPastNotificationsByDataType(amount: Int, dataType: HealthDataType, completion: (result: [Notification]) -> ()) {
        self.realm = try! Realm()
        var entries: [Notification] = []
        var i = 0
        for object in self.realm.objects(Notification).sorted("timestamp", ascending: false) {
            if i < amount && object.getDataType() == dataType && (object.getDate()?.timeIntervalSinceNow.isSignMinus)! {
                i += 1
                entries.append(object)
            }
        }
        completion(result: entries)
    }
    
    //Gets the amount of Notifications triggeres by data type
    func getNotificationAmountFromDateByDataType(fromDate: NSDate, dataType: HealthDataType, completion: (amount: Int) -> ()) {
        self.realm = try! Realm()
        var amount = 0
        let now = NSDate()
        for object in self.realm.objects(Notification).sorted("timestamp", ascending: false) {
            print((object.getDate()?.timeIntervalSinceDate(fromDate))!)
            if object.getDataType() == dataType && !(object.getDate()?.timeIntervalSinceDate(fromDate).isSignMinus)! && (object.getDate()?.timeIntervalSinceDate(now).isSignMinus)! {
                amount += 1
            }
            
        }
        completion(amount: amount)
    }
    
    //Deletes a specific notification from Realm
    func deleteNotification(index: Int, dataType: HealthDataType) {
        self.realm = try! Realm()
        let objects = self.realm.objects(Notification).filter("id = '" + String(index) + dataType.rawValue + "'")
        
        print("to be deleted" + String(objects))
        for object in objects {
        try! self.realm.write {
            self.realm.delete(object)
            }
        }
        
    }
    
    
}
