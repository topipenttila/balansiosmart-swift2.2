//
//  Notification.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 10/12/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import RealmSwift
import HealthKit

enum NotificationType: String {
    case Reminder = "Reminder"
    case Motivational = "Motivational"
    case ChangeGoal = "Goal"
}

// Class for a Notification Realm object

class Notification: Object {
    
    private dynamic var id: String = ""
    private dynamic var rawDataType = ""
    internal var dataType: HealthDataType {
        get {
            return HealthDataType(rawValue: rawDataType)!
        }
        set {
            rawDataType = newValue.rawValue
        }
    }
    private dynamic var rawNotificationType = ""
    internal var notificationType: NotificationType {
        get {
            return NotificationType(rawValue: rawNotificationType)!
        }
        set {
            rawNotificationType = newValue.rawValue
        }
    }
    private dynamic var timestamp: NSDate?
    
    func set(id: String, dataType: HealthDataType, timestamp: NSDate, notificationType: NotificationType) {
        self.id = id
        self.dataType = dataType
        self.timestamp = timestamp
        self.notificationType = notificationType
    }
    
    func getId() -> String {
        return self.id
    }
    
    func getDataType() -> HealthDataType {
        return self.dataType
    }
    
    func getDate() -> NSDate? {
        return self.timestamp
    }
    
    
    func getNotificationType() -> NotificationType {
        return self.notificationType
    }
    
    func save() {
        let realm = try! Realm()
        try! realm.write {
            realm.add(self)
        }
    }
    
    override static func ignoredProperties() -> [String] {
        return ["dataType"]
    }
}
