//
//  WalkthroughViewController.swift
//  BalansioSmartXcode7
//
//  Created by iosdev on 22.11.2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit

class WalkthroughViewController: UIViewController {

    @IBOutlet weak var headerLabel:UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var customButton: UIButton!
    @IBOutlet weak var defaultButton: UIButton!
    
    // MARK: - Data model for each walkthrough screen
    var index = 0               // the current page index
    var headerText = ""
    var imageName = ""
    var descriptionText = ""
    
    //Hides statusbar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerLabel.text = headerText
        descriptionLabel.text = descriptionText
        imageView.image = UIImage(named: imageName)
        pageControl.currentPage = index
        
        // customize buttons
        customButton.hidden = (index == 3) ? false : true
        defaultButton.hidden = (index == 3) ? false : true
        customButton.layer.cornerRadius = 5.0
        customButton.layer.masksToBounds = true
        defaultButton.layer.cornerRadius = 5.0
        defaultButton.layer.masksToBounds = true
        defaultButton.setTitle(NSLocalizedString("default_goals", comment: ""), forState: .Normal)
        customButton.setTitle(NSLocalizedString("custom_goals", comment: ""), forState: .Normal)
    }
    
    //When goals is clicked, you wont see walkthrough again
    //Custom goals function
    @IBAction func customClicked(sender: AnyObject)
    {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setBool(true, forKey: "DisplayedWalkthrough")
        
        let progressViewNavigationController = UIStoryboard(name: "ProgressView", bundle: nil).instantiateInitialViewController() as! UINavigationController
        let window = UIApplication.sharedApplication().delegate?.window
        if let window = window {
            window!.rootViewController = progressViewNavigationController
            //progressVC?.performSegueWithIdentifier("FromProgressViewToGoalComposer", sender: nil)
            progressViewNavigationController.viewControllers.first?.performSegueWithIdentifier("FromProgressViewToGoalComposer", sender: nil)
        }
    }
    //Default goals function
    @IBAction func defaultClicked(sender: AnyObject) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setBool(true, forKey: "DisplayedWalkthrough")
        
        DbUtils.generateDefaultGoals()
        
        let progressViewNavigationController = UIStoryboard(name: "ProgressView", bundle: nil).instantiateInitialViewController()
        let window = UIApplication.sharedApplication().delegate?.window
        if let window = window {
            window!.rootViewController = progressViewNavigationController
        }
        
        
        
    }
    
    
}

























