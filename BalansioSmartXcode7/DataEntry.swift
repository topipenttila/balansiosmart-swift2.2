//
//  DataEntry.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 31/10/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import RealmSwift
import HealthKit

// A Single DataEntry Realm Object from HealthKit

class DataEntry: Object {
    
    private dynamic var rawDataType = ""
    internal var dataType: HealthDataType {
        get {
            return HealthDataType(rawValue: rawDataType)!
        }
        set {
            rawDataType = newValue.rawValue
        }
    }
    private dynamic var timestamp: NSDate?
    private var value = RealmOptional<Double>()
    private dynamic var unit: String?
    
    func set(dataType: HealthDataType, timestamp: NSDate, value: Double, unit: String) -> DataEntry {
        
        self.dataType = dataType
        self.timestamp = timestamp
        self.value = RealmOptional<Double>(value)
        self.unit = unit
        
        return self
    }
    
    func getDataType() -> HealthDataType {
        return self.dataType
    }
    
    func getTimestamp() -> NSDate? {
        return self.timestamp
    }
    func getValue() -> Double? {
        return self.value.value
    }
    func getUnit() -> String? {
        return self.unit
    }
    
    func getPeriodByDataType(dataType: HealthDataType) -> Period? {
        // Get the default Realm
        
        let realm = try! Realm()
        let list = realm.objects(Goal)
        for i in list {
            if i.getDataType() == dataType {
                return i.getDiscipline()!.period
            }
        }
        
        return nil
    }
    
    func getAmountByDataType(dataType: HealthDataType) -> Int? {
        // Get the default Realm
        let realm = try! Realm()
        let list = realm.objects(Goal)
        for i in list {
            if i.getDataType() == dataType {
                return i.getDiscipline()!.getAmount()
            }
        }
        return nil
    }
    
    func save() {
        let realm = try! Realm()
        try! realm.write {
            realm.add(self)
        }

        
    }
    
    override static func ignoredProperties() -> [String] {
        return ["dataType"]
    }
    
    
    // Specify properties to ignore (Realm won't persist these)
    
    //  override static func ignoredProperties() -> [String] {
    //    return []
    //  }
}
