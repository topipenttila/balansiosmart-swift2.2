//
//  Clinical.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 01/11/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import RealmSwift
import HealthKit

class Clinical: Object {
    
    private dynamic var min: Double = 0.0
    private dynamic var max: Double = 0.0
    private dynamic var unit: String?
    
    func getMin() -> Double {
        return self.min
    }
    func getMax() -> Double {
        return self.max
    }
    func getUnit() -> String? {
        return self.unit
    }
    
    func set(min: Double, max: Double, unit: String) -> Clinical? {
        
        //Return nil if no valid values
        if min <= max {
            self.min = min
            self.max = max
            self.unit = unit
            return self
        } else {
            print("Invalid values")
        }
        
        return nil
    }
    
}
